const uuid = require('node-uuid')
const bunyan = require('bunyan')

module.exports = (config, req) => {
  config.type = 'application-log'
  if (req) {
    config.correlationId = req.headers['x-request-correlation-id'] || uuid.v4()
    config.appId = req.headers['x-application-id']
  }

  if (!config.correlationId) {
    config.correlationId = uuid.v4()
  }

  const logger = bunyan.createLogger(config)
  const debugLogger = logger.child({ severity: 'debug' })
  const infoLogger = logger.child({ severity: 'info' })
  const warnLogger = logger.child({ severity: 'warn' })
  const errorLogger = logger.child({ severity: 'error' })
  const fatalLogger = logger.child({ severity: 'fatal' })

  const logProxy = {
    debug: debugLogger.debug.bind(debugLogger),
    info: infoLogger.info.bind(infoLogger),
    warn: warnLogger.warn.bind(warnLogger),
    error: errorLogger.error.bind(errorLogger),
    fatal: fatalLogger.fatal.bind(fatalLogger)
  }

  if (req) {
    req.log = logProxy
    req.correlationId = config.correlationId
    req.appId = config.appId
  }

  return logProxy
}
